FROM ubuntu:18.04

# Scripted Benchmark with Wolfram Engine backend

ARG SBT_VERSION=1.3.7
ARG KYX_VERSION_STRING=480
ARG WOLFRAM_ENGINE_PATH=/usr/local/Wolfram/WolframEngine/12.1
ARG DEBIAN_FRONTEND="noninteractive"
ENV TZ=America/New_York

# Install requirements
RUN apt-get -y update && apt-get -y upgrade && apt-get -y install \
    apt-utils \
    software-properties-common \
    curl \
    avahi-daemon \
    wget \
    unzip \
    zip \
    build-essential \
    openjdk-8-jre-headless \
    openjdk-8-jdk \
    git \
    sshpass \
    sudo \
    locales \
    locales-all \
    ssh \
    vim \
    expect \
    libfontconfig1 \
    libgl1-mesa-glx \
    libasound2 \
    && rm -rf /var/lib/apt/lists/*

RUN systemctl enable avahi-daemon

# Install Scala
RUN wget -P /tmp https://www.scala-lang.org/files/archive/scala-2.12.8.deb
WORKDIR /tmp/
RUN dpkg -i scala-2.12.8.deb
RUN apt-get -y update && apt-get -y upgrade && apt-get -y install scala

# Install SBT
RUN wget https://bintray.com/artifact/download/sbt/debian/sbt-$SBT_VERSION.deb
RUN dpkg -i sbt-$SBT_VERSION.deb
RUN apt-get -y update && apt-get -y upgrade && apt-get -y install sbt

# Install Wolfram Engine
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && locale-gen
RUN wget https://account.wolfram.com/download/public/wolfram-engine/desktop/LINUX && sudo bash LINUX -- -auto -verbose && rm LINUX

# Create benchmark directory
WORKDIR /root/
RUN mkdir arch2020
RUN mkdir arch2020/kyx$KYX_VERSION_STRING

# Pull KeYmaera X
WORKDIR /root/
RUN git clone -b arch2020 --depth 1 https://github.com/LS-Lab/KeYmaeraX-release.git

# Build KeYmaera X
WORKDIR /root/KeYmaeraX-release/
RUN echo "mathematica.jlink.path=$WOLFRAM_ENGINE_PATH/SystemFiles/Links/JLink/JLink.jar" > local.properties
RUN sbt clean assembly

# Copy KeYmaera X to benchmark directory
WORKDIR /root/KeYmaeraX-release/
RUN cp keymaerax-webui/target/scala-2.12/KeYmaeraX*.jar /root/arch2020/keymaerax_480.jar

# Import benchmark index and script
WORKDIR /root/arch2020
COPY index480/ ./index480/
ADD *.xml ./
ADD runKeYmaeraX480ScriptedBenchmarks ./

# Pull KeYmaera X Projects
WORKDIR /root/
RUN git clone -b arch2020 --depth 1 https://github.com/LS-Lab/KeYmaeraX-projects.git
RUN cp KeYmaeraX-projects/benchmarks/*.kyx /root/arch2020/kyx480/
