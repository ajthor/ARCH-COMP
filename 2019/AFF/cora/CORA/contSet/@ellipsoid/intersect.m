function [E] = intersect(varargin)
% intersect - intersect two ellipsoids or an ellipsoid and a hyperplane
%
% Syntax:  
%    [E] = intersect(E1,E2)
%    [E} = intersect(E1,H);
%    [E] = intersect(H,E2);
%
% Inputs:
%    E1,E2 - Ellipsoid object
%    H - Hyperplane object
%
% Outputs:
%    E - ellipsoid, representing external approximation of intersection
%
% Example: 
%    E1=ellipsoid([1 0; 0 1]);
%    E2=ellipsoid([1 1; 1 1]);
%    E =intersect(E1,E2);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Matthias Althoff
% Written:      30-September-2006 
% Last update:  07-September-2007
%               05-January-2009
%               06-August-2010
%               01-February-2011
%               08-February-2011
%               18-November-2015
% Last revision:---

%------------- BEGIN CODE --------------
%NOTICE: this uses the symbolic toolbox and thus is VERY slow. This is just
%a quick mockup to test if it works in principle
E1=[];
E2=[];
if nargin~=2
    error('This function takes exactly two inputs');
end
if isa(varargin{1},'ellipsoid') && isa(varargin{2},'ellipsoid')
    E1 = varargin{1};
    E2 = varargin{2};
else
    error('Wrong input arguments');
end

if E1.isdegenerate || E2.isdegenerate
    error('E1/E2 degenerate, therefore intersection empty')
elseif length(E1.Q)~=length(E2.Q)
    error('E1/E2 have to have same dimensions');
end
W1 = inv(E1.Q);
W2 = inv(E2.Q);
q1 = E1.q;
q2 = E2.q;
n = length(W1);
syms p 
assume(p,'real');
assumeAlso(p>=0);
assumeAlso(p<=1);
X = p*W1+(1-p)*W2;
alpha = 1-p*(1-p)*((q2-q1)'*W2*inv(X)*W1*(q2-q1));
qp = inv(X)*(p*W1*q1+(1-p)*W2*q2);
eq = alpha*det(X)^2*trace(inv(X)*(W1-W2)) - n*det(X)^2*(2*qp'*(W1*q1-W2*q2)+qp'*(W2-W1)*qp - q1'*W1*q1+q2'*W2*q2)==0;
res = solve(eq,p);
alpha = subs(alpha,p,res);
X = subs(X,p,res);
Q = double(alpha*inv(X));
q = double(subs(qp,p,res));
E = ellipsoid(Q,q);
    %------------- END OF CODE --------------