function res = isequal(E1,E2)
% isequal - checks if ellipsoids E1,E2 are equal within E1.TOL
%
% Syntax:  
%    B = isequal(E,Y) gives a logical indicating whether E1,E2 are equal
%    (within E1.TOL)
% Inputs:
%    E1 - ellipsoids object
%    E2 - ellipsoids object
%
% Outputs:
%    res - boolean value indicating whether
%    points E1,E2 are equal
%
% Example: 
%    E1 = ellipsoid([1,0;0,1/2],[1;1]);
%    E2 = ellipsoid([1+1e-15,0;0,1/2],[1;1]);
%    B = equal(E1,E2);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Matthias Althoff
% Written:      27-July-2016
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
res_q = all(abs(E1.q-E2.q)<=E1.TOL);
res_Q = all(all(abs(E1.Q-E2.Q)<=E1.TOL));
res = res_q && res_Q;
%------------- END OF CODE --------------