
%NOTE: Run THIS Script for "non-reactive" .mat result files
    % if file name includes "reactive" then run other process script

%FIRST: load the .mat results file to be processed and then run the script
nT_SOAR=[]; %store number of simulations of falsifying runs
nR_SOAR=[]; %store lowest robustness of non-falsifying runs
numOfFals_SOAR=0; 
Fals_Inps_SOAR=[]; %store the falsifying input of each falsifying run
outer_runs_SOAR = length(history);

runtimes_SOAR=[];
for iii=1:outer_runs_SOAR
    if results.run(iii).falsified==1
        Fals_Inps_SOAR = [Fals_Inps_SOAR; results.run(iii).bestSample'];
        nT_SOAR=[nT_SOAR, results.run(iii).nTests];
        numOfFals_SOAR=numOfFals_SOAR+1;
    else
        nR_SOAR=[nR_SOAR; results.run(iii).bestRob];
    end
    runtimes_SOAR=[runtimes_SOAR;results.run(iii).time];
end

disp(['number of falsifications: ',num2str(numOfFals_SOAR),'/50'])
disp(['Average number of runs to falsify: ', num2str(mean(nT_SOAR))])
disp(['Median number of runs to falsify: ', num2str(median(nT_SOAR))])