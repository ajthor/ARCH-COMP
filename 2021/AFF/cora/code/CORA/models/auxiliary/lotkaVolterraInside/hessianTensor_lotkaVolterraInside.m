function Hf=hessianTensor_lotkaVolterraInside(x,u)



 Hf{1} = sparse(4,4);

Hf{1}(2,1) = -3;
Hf{1}(1,2) = -3;


 Hf{2} = sparse(4,4);

Hf{2}(2,1) = 1;
Hf{2}(1,2) = 1;


 Hf{3} = sparse(4,4);

