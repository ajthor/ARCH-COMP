function [c]=segmentCenter(varargin)
% segmentIntervals: returns a cell array of cell center positions of the
% partition segments whose indices are given as input
%
% Input:
% 1st input:            Interval object
% 2nd input (optional): segment indices as vector
%
% Output:
% c:                    Cell array of cell centers
%
% Checked:  1.8.17, AP

%cases for one or two input arguments------------------
if nargin==1
    Obj=varargin{1};
    cellNrs=Obj.actualSegmentNr;
elseif nargin==2
    Obj=varargin{1};
    cellNrs=varargin{2};
else
    disp('segmentIntervals: wrong number of inputs');
    return
end
%-------------------------------------------------------

% check the demanded cells are within limits...
if ~(prod(cellNrs > 0)&&prod(cellNrs<=prod(Obj.nrOfSegments)))
    disp('some cells are out of bounds');
    c = [];
    return
end

% Get subscripts out of the segment number 
subscripts=i2s(Obj,cellNrs);

for i = 1:size(subscripts,1)
    for j = 1:size(subscripts,2)
        center(j,1) = (Obj.dividers{j}(subscripts(i,j))+Obj.dividers{j}(subscripts(i,j)+1))/2;
    end
    c{i} = center;
end